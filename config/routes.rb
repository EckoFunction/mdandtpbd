Rails.application.routes.draw do
	root "index#index"
	resources :terms

	match "/404" => "index#error404", via: [ :get, :post, :patch, :delete ]
	match "/panel" => "index#panel", via: [ :get ]
	match "/panel" => "index#manage", via: [ :post ]
end
