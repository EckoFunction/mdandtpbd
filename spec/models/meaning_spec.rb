require 'rails_helper'

describe Meaning do

	describe "Associations" do
		it { should respond_to(:content) }

		it { should belong_to(:term) }
	end

	describe "Validations" do
		it { should validate_presence_of :content }
	end
end