require 'rails_helper'

describe Declension do

	describe "Associations" do
		it { should respond_to(:content) }

		it { should have_many(:terms) }
	end

	describe "Validation" do
		it { should validate_presence_of :content }
		it { should validate_uniqueness_of :content }
		it { should validate_inclusion_of(:content).in_array(%w(first second third empty)) }
	end
end