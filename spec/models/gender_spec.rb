require 'rails_helper'

describe Gender do

	describe "Associations" do
		it { should have_many(:terms) }
	end

	describe "Validations" do
		it { should respond_to(:content) }

		it { should validate_inclusion_of(:content).in_array(%w(masculine feminine neuter empty)) }
		it { should validate_uniqueness_of(:content) }
	end

end