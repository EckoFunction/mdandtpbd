require 'rails_helper'

describe SimilarTerm do
	
	# toDO: проверка на реальный объект? validates_prescence_of(:object_name)
	it { should respond_to(:parent_term_id) }
	it { should respond_to(:child_term_id) }

	# toDO: стоит ли провеять моногик ко многим с обеих сторон?
	it { should belong_to(:parent_term) }
	it { should belong_to(:child_term) }
end