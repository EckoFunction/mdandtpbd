require "rails_helper"

describe TermNumber do
	
	describe "Associations" do
		it { should have_one(:term) }
	end

	describe "Validation" do
		it { should validate_presence_of :content }
		it { should validate_uniqueness_of :content }
		it { should validate_inclusion_of(:content).in_array(%w(singular plural empty)) }
		# toDO: узнать про have_readonly_attribute
	end
end