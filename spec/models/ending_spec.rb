require 'rails_helper'

describe Ending do

	describe "Associations" do
		it { should respond_to(:content) }

		it { should have_many(:terms) }
	end

	describe "Validation" do
		it { should validate_presence_of :content }
		it { should validate_uniqueness_of :content }
	end
end