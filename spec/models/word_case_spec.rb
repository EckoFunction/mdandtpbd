require "rails_helper"

describe WordCase do

	it { should respond_to(:content) } 

	it { should belong_to(:declension) }
end