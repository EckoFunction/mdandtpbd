require 'rails_helper'

describe Term do
	
	describe "Associations and depenencies"  do

		it { should respond_to(:content) }

		it { should have_many(:meanings).dependent(:destroy) }
		it { should have_and_belong_to_many(:similar_terms)}
		it { should belong_to(:declension) }
		it { should belong_to(:gender) }
		it { should belong_to(:term_number) }
		it { should belong_to(:ending) }
	end

	describe "Validations" do

		# Некорректно проверилось значение по регулярке
		it { should validate_presence_of :content }
		it { should validate_uniqueness_of :content }
	end
end