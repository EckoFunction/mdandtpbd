require 'rails_helper'

describe "Ключевые страницы" do

	describe "Главная страница" do

		it "должна содержать конент - \"Словарь Ожегова\"" do
			visit '/'
			expect(page).to have_content(I18n.t('titles.page_titles.index.index'))
		end
	end
end