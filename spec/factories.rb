FactoryGirl.define do
	
	factory :meaning do
		sequence(:content) { |value|
			random_cyrillic_string
		}
		term
	end

	factory :term do
		sequence(:content, "а") { |value| "Тестовое#{ value }" }
		gender
		declension
		term_number

		factory :term_with_meanings do
			transient do
				meanings_count 3
			end

			after(:create) do | term , evaluator |
				create_list(:meaning, evaluator.meanings_count, term: term)
			end	
		end
	end


	factory :gender do
		content "masculine"
	end

	factory :declension do
		content "first"
	end

	factory :word_case do
		content "тестовый падеж"
	end

	factory :ending do
		content "а"
	end

	factory :term_number do
		content "singular"
	end

	# factory :similar_term do
	# 	first_term nil
	# 	second_term nil
	# end
end

def random_cyrillic_string
	return ("а".."я").to_a.shuffle.join
end