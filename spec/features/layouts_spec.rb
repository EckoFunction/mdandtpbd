require "rails_helper"

feature "all pages content testing" do
	let!(:term) { create :term }
	
	scenario "each page should have the right menu" do
		# toDO: как протестироват все страницы автоматически?
		paths = [ root_path, terms_path, term_path(term) , edit_term_path(term) , new_term_path ]

		paths.each do | path |
			visit path

			expect(page).to have_selector 'ul.site-menu li', :text => I18n.t("titles.menu_titles.create_term"), :visible => true
			expect(page).to have_selector 'ul.site-menu li', :text => I18n.t("titles.menu_titles.terms_list"), :visible => true
		end
	end

	scenario "click to menu item should pass user to right page" do

		visit root_path

		click_link I18n.t("titles.menu_titles.terms_list")

		expect(page).to have_title(I18n.t("titles.page_titles.index.index") + " - " + I18n.t("titles.page_titles.terms.index"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.index"))

		click_link I18n.t("titles.menu_titles.create_term")

		expect(page).to have_title(I18n.t("titles.page_titles.index.index") + " - " + I18n.t("titles.page_titles.terms.create"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.create"))

	end
end