require 'rails_helper'

feature "404 page" do
	scenario "user visit wrong page and see 404-page" do
		visit "/404"

		expect(page.status_code).to eq 404
	end
end