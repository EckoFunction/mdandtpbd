require 'rails_helper'

feature "Edit one term" do
	let!(:masculine_gender) { create :gender}
	let!(:empty_gender) { create :gender, :content => "empty" }
	let!(:feminine_gender) { create :gender, :content => "feminine" }
	let!(:neuter_gender) { create :gender, :content => "neuter" }

	let!(:empty_declension) { create :declension, :content => "empty" }
	let!(:first_declension) { create :declension, :content => "first" }
	let!(:second_declension) { create :declension, :content => "second" }
	let!(:third_declension) { create :declension, :content => "third" }

	let!(:empty_term_number) { create :term_number, :content => "empty" }
	let!(:singular_term_number) { create :term_number, :content => "singular" }
	let!(:plural_term_number) { create :term_number, :content => "plural" }
	let!(:term) { create(:term_with_meanings, {
			:gender => masculine_gender,
			:declension => first_declension,
			:term_number => singular_term_number,
		}
	)}
	before { visit edit_term_path(term) }

	scenario "The page should have the right title and content" do

		expect(page).to have_title(I18n.t('titles.page_titles.index.index') + " - " + I18n.t('titles.page_titles.terms.edit'))
		expect(page).to have_title(I18n.t('titles.page_titles.terms.edit'))
	end

	scenario "User submit form with wrong values" do
		within ".edit_form" do
			fill_in 'term_content', :with => ""
		end

		for i in 0..(term.meanings.count-1) do
			within ".edit_form" do
				fill_in "term_meanings_attributes_" + i.to_s + "_content", { :with => "" }
			end
		end	

		click_button I18n.t("titles.buttons.submit")

		if term.errors.any?
			term.errors.each do | field , error |
				expect(page).to have_content error
			end
		end
	end
 
	scenario "Fill in the inputs and submit them" do
		within ".edit_form" do
			fill_in 'term_content', :with => "Измененное слово"
		end
		select I18n.t('models.gender.attributes.content.values.masculine'), :from => "term_gender_id"
		select I18n.t('models.declension.attributes.content.values.empty'), :from => "term_declension_id"
		select I18n.t('models.term_number.attributes.content.values.plural'), :from => "term_term_number_id"

		for i in 0..(term.meanings.count-1) do
			within ".edit_form" do
				fill_in "term_meanings_attributes_" + i.to_s + "_content", {
					:with => "this is test content for test_meaning_" + i.to_s
				}
			end
		end

		click_button I18n.t("titles.buttons.submit")

		# toDO: или стоит проверять через статичный текст?
		expect(page).to have_content("Измененное слово")
		expect(page).to have_content(I18n.t('models.gender.attributes.content.values.masculine'))
		expect(page).to have_content(I18n.t('models.declension.attributes.content.values.empty'))
		expect(page).to have_content(I18n.t('models.term_number.attributes.content.values.plural'))
		for i in 0..(term.meanings.count-1) do
			expect(page).to have_content "this is test content for test_meaning_" + i.to_s
		end
		# expect(page).to have_content(term.content)
		# expect(page).to have_content(I18n.t(term.gender.content))
		# expect(page).to have_content(I18n.t(term.declension.content))
		# expect(page).to have_content(I18n.t(term.term_number.content))
		# term.meanings.each do | meaning |
		# 	expect(page).to have_content meaning.content
		# end
	end	
end