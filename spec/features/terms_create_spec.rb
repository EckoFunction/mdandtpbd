require 'rails_helper'

feature "Create single term" do

	let!(:masculine_gender) { create :gender }
	let!(:femiline_gender) { create :gender, { :content => "feminine" } }
	let!(:neuter_gender) { create :gender, { :content => "neuter" } }
	let!(:empty_gender) { create :gender, { :content => "empty" } }

	let!(:first_declension) { create :declension }
	let!(:second_declension) { create :declension, { :content => "second" } }
	let!(:third_declension) { create :declension, { :content => "third" } }
	let!(:empty_declension) { create :declension, { :content => "empty" } }

	let!(:singular_term_number) { create :term_number }
	let!(:plural_term_number) { create :term_number, { :content => "plural" } }
	let!(:empty_term_number) { create :term_number, { :content => "empty" } }

	before { visit new_term_path }

	scenario "create page should have the right titles" do

		expect(page).to have_title(I18n.t('titles.page_titles.index.index') + " - " + I18n.t('titles.page_titles.terms.new'))
		expect(page).to have_content(I18n.t('titles.page_titles.terms.new'))
	end

	scenario "User submit empty form" do 
		click_button I18n.t("titles.buttons.submit")

		term = Term.new
		term.save

		if term.errors.any?
			term.errors.each do | field , error |
				expect(page).to have_content error
			end
		end
	end

	scenario "User filled inputs and submit the form" do
		within ".edit_form" do
			fill_in "term_content", :with => "Новое слово"

			select I18n.t("models.gender.attributes.content.values.feminine"), :from => "term_gender_id"
			select I18n.t("models.declension.attributes.content.values.third"), :from => "term_declension_id"
			select I18n.t("models.term_number.attributes.content.values.empty"), :from => "term_term_number_id"
			# toDo: а что если полей много?
			fill_in "term[meanings_attributes][0][content]", :with => "this is the test meaning for new term"
		end

		click_button I18n.t("titles.buttons.submit")

		expect(page).to have_content "Новое слово"
		expect(page).to have_content I18n.t("models.gender.attributes.content.values.feminine")
		expect(page).to have_content I18n.t("models.declension.attributes.content.values.third")
		expect(page).to have_content I18n.t("models.term_number.attributes.content.values.empty")
		expect(page).to have_content "this is the test meaning for new term"
	end

end