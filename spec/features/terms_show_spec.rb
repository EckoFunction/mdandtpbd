require "rails_helper"

feature "Show term data" do
	let!(:gender) { create(:gender) }
	let!(:declension) { create(:declension) }
	let!(:ending) { create(:ending) }
	let!(:term_number) { create(:term_number) }
	let!(:term) { create :term, {
			:gender => gender,
			:declension => declension,
			:term_number => term_number
		} 
	}

	scenario "page should have the right titile and page-title" do
		visit term_path(term)

		expect(page).to have_title(I18n.t("titles.page_titles.index.index") + " - " + I18n.t("titles.page_titles.terms.show"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.show"))
	end

	scenario "page should have the right content with full filled term" do
		visit term_path(term)

		expect(page).to have_content(I18n.t("models.term.one"))
		expect(page).to have_content(I18n.t("models.gender.one"))
		expect(page).to have_content(I18n.t("models.declension.one"))
		expect(page).to have_content(I18n.t("models.term_number.one"))
		expect(page).to have_content(I18n.t("models.meaning.many"))

		expect(page).to have_content(term.content)
		expect(page).to have_content(I18n.t("models.gender.attributes.content.values.#{term.gender.content}"))
		expect(page).to have_content(I18n.t("models.declension.attributes.content.values.#{term.declension.content}"))
		expect(page).to have_content(I18n.t("models.term_number.attributes.content.values.#{term.term_number.content}"))

		term.meanings.each do |meaning|
			expect(page).to have_content(meaning.content)
		end

		expect(page).to have_link I18n.t("titles.buttons.edit"), :href => edit_term_path(term)
		expect(page).to have_link I18n.t("titles.buttons.delete"), :href => term_path(term)
	end

	scenario "Click to edit-link, should pass user to edit-page" do
		visit term_path(term)

		click_link I18n.t("titles.buttons.edit")

		expect(page).to have_title(I18n.t("titles.page_titles.index.index") + " - " + I18n.t("titles.page_titles.terms.edit"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.edit"))
	end
	
	scenario "Click to delete link should delete the Term and show the right flash-message" do
		visit term_path(term)

		click_link I18n.t("titles.buttons.delete")

		expect(page).to have_title(I18n.t("titles.page_titles.index.index") + " - " + I18n.t("titles.page_titles.terms.index"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.index"))

		expect(page).not_to have_link term.content, :href => term_path(term)

		expect(page).to have_content I18n.t("messages.term.delete.success", :word => term.content)
		expect(page).to have_selector '.flash.success', :text => I18n.t("messages.term.delete.success", :word => term.content), :visible => true
	end

	scenario "Click to already deleted link should render terms-list page and show the right flash-message" do
		visit term_path(term)

		term.destroy

		click_link I18n.t("titles.buttons.delete")

		expect(page).to have_title(I18n.t("titles.page_titles.index.index") + " - " + I18n.t("titles.page_titles.terms.index"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.index"))

		expect(page).not_to have_link term.content, :href => term_path(term)

		expect(page).to have_content I18n.t("messages.term.delete.failed", :word => term.content)
		expect(page).to have_selector '.flash.failed', :text => I18n.t("messages.term.delete.failed"), :visible => true		
	end
end