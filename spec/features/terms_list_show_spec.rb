require "rails_helper"

feature "Show terms list" do
	# создать несколько слов
	let!(:gender) { create :gender }
	let!(:declension) { create :declension }
	let!(:term_number) { create :term_number }
	let!(:term) { create :term_with_meanings, {
			gender: gender, 
			declension: declension, 
			term_number: term_number
		}
	}

	scenario "page should have the right content and title" do
		visit terms_path


		expect(page).to have_title(I18n.t("titles.page_titles.terms.index"))
		expect(page).to have_content(I18n.t("titles.page_titles.terms.index"))
	end

	scenario "page should have the right links, link-texts and translation" do
		visit terms_path


		Term.all.each do | term |
			expect(page).to have_link(term.content, term_path(term))
			expect(page).to have_content(I18n.t("models.declension.attributes.content.values.#{term.declension.content}"))
			expect(page).to have_content(I18n.t("models.gender.attributes.content.values.#{term.gender.content}"))
			expect(page).to have_content(I18n.t("models.term_number.attributes.content.values.#{term.term_number.content}"))
		end

	end

	scenario "page should have the right content when there are no terms" do
		Term.all.each do |term|
			term.destroy
		end

		visit terms_path

		expect(page).to have_content(I18n.t('models.term.empty'))
	end
end