class Ending < ActiveRecord::Base
	has_many :terms

	validates :content, :presence => true, :uniqueness => true
end
