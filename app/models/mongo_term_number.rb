class MongoTermNumber
  include Mongoid::Document

  field :term_number, type: String
  index "term_number" => 1

	has_many :associated_mongo_term
	TERM_NUMBER_INCLUSIONS = %w{singular plural empty}

	validates :term_number, {
		:presence => true , 
		:inclusion => { :in => TERM_NUMBER_INCLUSIONS }
	}
end
