class AssociatedMongoTerm
  include Mongoid::Document

  field :term, type: String
  index "term" => 1

	has_many :mongo_meanings, :dependent => :destroy
	belongs_to :mongo_declension
	belongs_to :mongo_gender
	belongs_to :mongo_term_number
	belongs_to :mongo_ending

	# accepts_nested_attributes_for :meanings
	
	# TERM_FORMAT = /\A([А-Яа-яЁё][А-Яа-яЁё ]*)+\z/
	# validates :term, :presence => true, :format => TERM_FORMAT, :uniqueness => true

end
