class Meaning < ActiveRecord::Base
	belongs_to :term

	validates :content, :presence => true
end
