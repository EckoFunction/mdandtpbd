class SimilarTerm < ActiveRecord::Base
	belongs_to :parent_term, {
		:foreign_key => "parent_term_id",
		:class_name => "Term",
	}
	belongs_to :child_term, {
		:foreign_key => "child_term_id",
		:class_name => "Term",
	}
end
