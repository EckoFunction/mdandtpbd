class MongoEnding
  include Mongoid::Document

  field :ending, type: String
  index "ending" => 1

	has_many :associated_mongo_term

	# ENDING_FORMAT = /\A[а-яё]+\z/

	# validates :ending, :presence => true, :format => ENDING_FORMAT
end
