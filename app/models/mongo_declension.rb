class MongoDeclension
  include Mongoid::Document

  field :declension, type: String
  index "declension" => 1

	has_many :associated_mongo_term, :dependent => :destroy
	
	ENDING_FORMAT = /\A[а-яё]+\z/

	DECLENSION_INCLUSIONS = %w(first second third empty)

	validates :declension, {
		:presence => true, 
		:inclusion => { :in => DECLENSION_INCLUSIONS },
	}

end
