class Term < ActiveRecord::Base
	CONTENT_FORMAT = /\A([А-Яа-яЁё][А-Яа-яЁё ]*)+\z/

	has_many :meanings, :dependent => :destroy
	belongs_to :declension
	belongs_to :gender
	belongs_to :term_number
	belongs_to :ending

	accepts_nested_attributes_for :meanings

	has_and_belongs_to_many( :similar_terms, {
			:class_name => "Term",
			:join_table => "similar_terms",
			:foreign_key => "parent_term_id",	
			:association_foreign_key => "child_term_id",
		}
	)

	# validates :content, :presence => true, :format => CONTENT_FORMAT, :uniqueness => true
end
