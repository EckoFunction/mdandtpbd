class MongoTerm
  include Mongoid::Document

	TERM_FORMAT = /\A([А-Яа-яЁё][А-Яа-яЁё ]*)+\z/
	# ENDING_FORMAT = /\A[а-яё]+\z/

	DECLENSION_INCLUSIONS = %w(first second third empty)
	GENDER_INCLUSIONS = %w{masculine feminine neuter empty}
	TERM_NUMBER_INCLUSIONS = %w{singular plural empty}

  field :term, type: String
  field :meanings, type: Array
  field :ending, type: String
  field :declension, type: String
  field :gender, type: String
  field :term_number, type: String

  index "term" => 1
  index "ending" => 1
  index "declension" => 1
  index "gender" => 1
  index "term_number" => 1

	# validates :term, :presence => true, :format => TERM_FORMAT, :uniqueness => true
	# validates :meanings, :presence => true
	# validates :ending, :presence => true, :format => ENDING_FORMAT
	validates :declension, {
		:presence => true, 
		:inclusion => { :in => DECLENSION_INCLUSIONS },
	}
	validates :gender, { 
		:presence => true,
		:inclusion => { :in => GENDER_INCLUSIONS },
	}
	validates :term_number, {
		:presence => true , 
		:inclusion => { :in => TERM_NUMBER_INCLUSIONS }
	}

end
