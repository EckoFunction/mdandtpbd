class Gender < ActiveRecord::Base
	has_many :terms

	validates :content, { 
		:presence => true,
		:inclusion => { :in => %w{masculine feminine neuter empty} },
		:uniqueness => true,
	}
end