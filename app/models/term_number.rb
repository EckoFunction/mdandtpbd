class TermNumber < ActiveRecord::Base
	has_many :terms

	validates :content, :presence => true , :uniqueness => true, :inclusion => { :in => %w{singular plural empty} }

end
