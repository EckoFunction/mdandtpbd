class MongoGender
  include Mongoid::Document

  field :gender, type: String
  index "gender" => 1

	has_many :associated_mongo_term, :dependent => :destroy
	
	GENDER_INCLUSIONS = %w{masculine feminine neuter empty}

	validates :gender, { 
		:presence => true,
		:inclusion => { :in => GENDER_INCLUSIONS },
	}

end
