class MongoMeaning
  include Mongoid::Document

  field :meaning, type: String
  index "meaning" => 1

	belongs_to :associated_mongo_term
	validates :meaning, :presence => true

end
