class Declension < ActiveRecord::Base
	has_many :terms

	validates :content, {
		:presence => true, 
		:uniqueness => true, 
		:inclusion => { :in => %w(first second third empty) },
	}
end
