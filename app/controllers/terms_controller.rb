class TermsController < ApplicationController

	def index
		@terms = Term.eager_load(:meanings, :declension, :gender, :term_number).page(params[:page]).order(:content)
	end

	def show
		@term = Term.eager_load(:meanings, :declension, :gender, :term_number).find(params[:id])
	end 

	def edit
		flash = nil
		# toDO: Рефактор
		@term = Term.eager_load(:meanings, :declension, :gender, :term_number).find(params[:id])
	end

	def update
		@term = Term.find(params[:id])
		if @term.update(term_params)
			flash[:status] = "success" 
			redirect_to term_path(@term)
		else
			flash[:status] = "failed"
			render :action => "edit"
		end
	end

	def destroy
		term = Term.find_by(:id => params[:id])
		flash[:status] = "failed"
		if term
				term.destroy
				flash[:status] = "success"
				flash[:message] = I18n.t("messages.term.delete.#{flash[:status]}", :word => term.content)
		else
			flash[:message] = I18n.t("messages.term.delete.#{flash[:status]}")
		end
		redirect_to terms_path
	end

	def new	
		@term = Term.new	
	end

	def create
		@term = Term.new(term_params)
		# toDo: допилить с учётом тестов
		if @term.save
			redirect_to terms_path
		else
			render :action => 'new'
		end
	end

	private 

		def term_params
			params.require(:term).permit(
				:content, 
				:gender_id, 
				:declension_id, 
				:term_number_id, 
				:meanings_attributes => [
					:id, 
					:content
				]
			)
		end
end