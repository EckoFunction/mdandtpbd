class IndexController < ApplicationController
  def index
  	# change to eagerload
    # @data = Term.eager_load(:meanings).eager_load(:term_number).eager_load(:gender).eager_load(:declension)
  end

  def error404
  	render :status => :not_found
  end

  def panel
  end 

  def manage
  	if !params[:query].nil? && !params[:query].empty?
      @query = params[:query]
	  	@connection = ActiveRecord::Base.establish_connection(
				:adapter  => Rails.configuration.database_configuration[Rails.env]["adapter"],
				:host     => Rails.configuration.database_configuration[Rails.env]["host"],
				:database => Rails.configuration.database_configuration[Rails.env]["database"],
				:username => Rails.configuration.database_configuration[Rails.env]["username"],
				:password => Rails.configuration.database_configuration[Rails.env]["password"],
			)
      begin
  			@results = @connection.connection.execute(params[:query])
      rescue ActiveRecord::StatementInvalid => error
        @sql_error = error
      end
		end
  	render :action => "panel"
  end

end