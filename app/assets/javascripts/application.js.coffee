//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(document).ready ->

	$("#add_new_meaning").on "click", ->
		meanings_count = $("#meanings_container textarea").length
		$("#meanings_container").append '<textarea name="term[meanings_attributes][' + meanings_count + '][content]" id="term_meanings_attributes_' + meanings_count + '_content"></textarea>'
		return false

	$("#delete_meaning").on "click", ->
		meanings_textareas = $("#meanings_container textarea")
		meanings_count = meanings_textareas.length
		if meanings_count > 1
			meanings_textareas[meanings_count-1].remove()
		return false
