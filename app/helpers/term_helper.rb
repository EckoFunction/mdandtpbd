module TermHelper
	
	def generate_data_for_select_tag (object)
		output = []
		object.all.each do | item |
				output << [
					I18n.t("models.#{object}.attributes.content.values.#{item.content}".underscore),
					item.id
				]
		end
		output
	end

	def nested_term_for_meanings(term)
		term.meanings = [ Meaning.new ] unless term.meanings.any?
		term
	end
	
end