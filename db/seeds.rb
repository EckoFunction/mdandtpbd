# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

genders = [
	'empty',
	"masculine",
	"feminine",
	"neuter",
]

total = []

total << Benchmark.measure {
	genders.each do | gender |
		Gender.where(:content => gender).first_or_create
		MongoGender.where(:gender => gender).first_or_create
	end
}.real

# puts total.last

puts "genders was imported successfuly"

term_numbers = [
	'empty',
	"singular",
	"plural",
]

total << Benchmark.measure {
	term_numbers.each do | term_number |
		TermNumber.where(:content => term_number).first_or_create
		MongoTermNumber.where(:term_number => term_number).first_or_create
	end
}.real

# puts total.last


puts "term numbers was imported successfuly"

endings = [
	"а",
	"я",
	"о",
	"е",
	'empty',
]

total << Benchmark.measure {
	endings.each do | ending |
		Ending.where(:content => ending).first_or_create
		MongoEnding.where(:ending => ending).first_or_create
	end
}.real

# puts total.last

puts "endings was imported successfuly"

declensions = [
	'empty',
	"first",
	"second",
	"third",
]

total << Benchmark.measure {
	declensions.each do | declension |
		Declension.where(:content => declension).first_or_create
		MongoDeclension.where(:declension => declension).first_or_create
	end
}.real

# puts total.last

puts "declensions was imported successfuly"

terms = {
	# 1 склонение = мужской/женский род, окончания -а -я; И.П., ед.число
	"Зима" => {
		:meanings => [
			"Самое холодное время года, наступающее вслед за осенью и сменяющееся весной.",
			"город (с 1917) в Российской Федерации, Иркутская обл., на р. Ока,близ впадения в нее р. Зима.
			Железнодорожная станция. 38,6 тыс. жителей(1992). Лесопильно-деревообрабатывающая промышленность. Химический завод;электрохимический комплекс.",
		],
		:gender => "feminine",
		:term_number => "singular",
		:ending => "а",
		:declension => "first",
	},
	"Дядя" => {
		:meanings => [
			"Брат отца или матери (по отношению к племянникам), муж тетки. ",
			"разг. Знакомый мужчина, старший по возрасту (обычно с именем собственным). ",
			"перен. разг. Тот, на кого возлагают надежды, от кого ожидают помощи, но кого обычно не существует в реальной действительности (обычно с оттенком шутливости). ",
		],
		:gender => "masculine",
		:term_number => "singular",
		:ending => "я",
		:declension => "first",
	},
	# 2 склонение, мужской род, нулевое окончание; средний/женский род окончания -о -е; И.П., ед.число
	"Стол" => {
		:meanings => [
			"Предмет мебели в виде широкой горизонтальной доски на одной или нескольких ножках (иногда с ящиками, тумбочками), на который кладут или ставят что-л.",
			"Такой предмет мебели вместе со всем, что поставлено на нем для еды.",
			"перен. Пища, кушанья, то, что подается для еды. // Особенности питания, режим питания.",
			"перен. Угощение на определенное число лиц (которое заказывают в ресторанах, кафе, столовых и т.п.).",
		],
		:gender => "masculine",
		:term_number => "singular",
		:ending => 'empty',
		:declension => "second",
	},
	"Яблоко" => {
		:meanings => [
			"Плод яблони. ",
			"в ботанике - сочный, обычно многосемянный нераскрывающийся плодрастений семейства розоцветных подсемейства яблоневых - груши, яблони,айвы, рябины и др. В быту яблоком называется плод яблони. ",
			"Означает плодородие, любовь, радость, знание, мудрость, обожествление и роскошь, но, вместе с тем, обманчивость и смерть. 
			Яблоко было запретным плодом Золотого Века. Будучи круглым, оно представляет целостность и единство и противопоставляется гранату, состоящему из множества зернышек. 
			Как плод Древа Жизни дается Иду-мой богам. Эрида бросила между богами золотое яблоко раздора. 
			Яблоко символизирует также бессмертие как плод из сада Гесперид или из сада Фрейи. Предложить яблоко - значит сделать признание в любви. 
			Подобно цветку апельсина (символ плодовитости), цветок яблони использовался как украшение для невест. 
			У кельтов Серебряная Ветвь - это ветвь яблони, обладающая магическими и хтоническими силами, а ее плод дает плодовитость невесте. 
			Яблочный праздник Хэллоуин связан со смертью старого года. У китайцев яблоко означает мир и согласие. В христианской традиции имеет двойственное значение. 
			С одной стороны означает зло (по-латински malum) и является плодом совращения Адама и Евы. С другой стороны, изображенный с Христом или Девой Марией, оно указывает на нового Адама и на спасение. 
			Обезьяна с яблоком во рту означает грехопадение. У греков яблоко посвящалось Венере как знак любви и желания; свадебный символ, олицетворяющий предложение руки и сердца. 
			Яблоко раздора Парисом было отдано Венере. Ветви яблони имеются в атрибутике Немезиды и Артемиды. 
			Используются также в обрядах, связанных с Дианой, где вручаются женихам в качестве приза в полуденных брачных соревнованиях в беге,
			(девушек-победительниц ночных брачных соревнований в беге награждали оливковой ветвью). Яблоком Диониса была айва. 
			Яблоневое дерево ассоциировалось со здоровьем и бессмертием. Посвящалось Аполлону. Цветок яблони - китайский символ мира и красоты. ",
		],
		:gender => "neuter",
		:term_number => "singular",
		:ending => "о",
		:declension => "second",
	},
	"Подмастерье" => {
		:meanings => [
			"Помощник, подручный мастера-ремесленника. ",
			"устар. Ремесленник, не имевший своей мастерской и работавший по найму. ",
		],
		:gender => "masculine",
		:term_number => "singular",
		:ending => "е",
		:declension => "second",
	},
	# 3 склонение, Женский род, нулевое окончание, мягкий знак на конце, И.П. ед.число
	"Радость" => {
		:meanings => [
			"Чувство удовольствия, внутреннего удовлетворения.",
			"разг. То, что возбуждает такое чувство. ",
			"перен. разг. Употр. как обращение к кому-л., соответствуя по знач. сл.: милый, любимый, дорогой.",
		],	
		:gender => "feminine",
		:term_number => "singular",
		:ending => 'empty',
		:declension => "third",
	},
	"Ночь" => {
		:meanings => [
			"Название части суток от захода до восхода солнца. ",
		],
		:gender => "feminine",
		:term_number => "singular",
		:ending => 'empty',
		:declension => "third",
	},
	"Тестовое слово" => {
		:meanings => nil,
		:gender => 'empty',
		:term_number => 'empty',
		:ending => 'empty',
		:declension => 'empty',
	},	
}


# total << Benchmark.measure {
# 	100.times do |i|
# 		terms.each do | term , attributes |
# 			new_word_gender = Gender.find_by(:content => attributes[:gender])
# 			new_term_number = TermNumber.find_by(:content => attributes[:term_number])
# 			new_term_ending = Ending.find_by(:content => attributes[:ending])
# 			new_term_declension = Declension.find_by(:content => attributes[:declension])

# 			new_term = Term.new(
# 				:content => term + i.to_s, 
# 				:gender => new_word_gender, 
# 				:term_number => new_term_number,
# 				:ending => new_term_ending,
# 				:declension => new_term_declension
# 			).save

# 			# new_term = Term.where({
# 			# 	:content => term, 
# 			# 	:gender => new_word_gender, 
# 			# 	:term_number => new_term_number,
# 			# 	:ending => new_term_ending,
# 			# 	:declension => new_term_declension
# 			# }).first_or_create	

# 			unless attributes[:meanings].nil?
# 				attributes[:meanings].each do | meaning |
# 					Meaning.where(:content => meaning, :term => new_term).first_or_create
# 				end
# 			end
# 		end
# 	end
# }.real

# puts total.last

# puts Benchmark.measure {
# 	100.times do |i|
# 		terms.each do | term , attributes |
# 			meanings = []

# 			unless attributes[:meanings].nil?
# 				attributes[:meanings].each do | meaning |
# 					meanings << meaning
# 				end
# 			end

# 			new_term = MongoTerm.new(
# 				:term => term, 
# 				:gender => attributes[:gender], 
# 				:term_number => attributes[:term_number],
# 				:ending => attributes[:ending],
# 				:declension => attributes[:declension],
# 				:meanings => meanings
# 			)
# 			new_term.save
# 			# if new_term.errors.any?
# 			# 	new_term.errors.full_messages.each do |error_message|
# 			# 		puts error_message
# 			# 	end
# 			# end
# 		end
# 	end
# }.real


puts Benchmark.measure {
	100.times do |i|
		terms.each do | term , attributes |
			meanings = []

			unless attributes[:meanings].nil?
				attributes[:meanings].each do | meaning |
					meanings << MongoMeaning.new(:meaning => meaning)
				end
			end

			new_term_gender = MongoGender.find_by(:gender => attributes[:gender])
			new_term_number = MongoTermNumber.find_by(:term_number => attributes[:term_number])
			new_term_ending = MongoEnding.find_by(:ending => attributes[:ending])
			new_term_declension = MongoDeclension.find_by(:declension => attributes[:declension])

			new_term = AssociatedMongoTerm.new(
				:term => term + i.to_s, 
				:mongo_gender => new_term_gender, 
				:mongo_term_number => new_term_number,
				:mongo_ending => new_term_ending,
				:mongo_declension => new_term_declension,
				:mongo_meanings => meanings
			)
			new_term.save
			# if new_term.errors.any?
			# 	new_term.errors.full_messages.each do |error_message|
			# 		puts error_message
			# 	end
			# end
		end
	end
}.real


puts "terms was imported successfuly"

# puts "total time is " + total.inject{ | sum, val | sum + val }.to_s