class CreateValues < ActiveRecord::Migration
  def change
    create_table :values do |t|
	t.text :value
	t.belongs_to :term, index: true
	
	t.timestamps
    end
  end
end
