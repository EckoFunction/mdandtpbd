class CreateTermNumber < ActiveRecord::Migration
  def change
    create_table :term_numbers do |t|
    	t.integer :content

		t.timestamps
    end
  end
end
