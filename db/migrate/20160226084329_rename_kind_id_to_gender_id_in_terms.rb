class RenameKindIdToGenderIdInTerms < ActiveRecord::Migration
  def change
  	rename_column :terms, :kind_id, :gender_id
  end
end
