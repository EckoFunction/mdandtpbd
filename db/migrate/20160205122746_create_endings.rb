class CreateEndings < ActiveRecord::Migration
  def change
    create_table :endings do |t|
		t.string :ending
		t.belongs_to :word_case, index: true
		
		t.timestamps
    end
  end
end
