class RenameKidsToGenders < ActiveRecord::Migration
  def change
  	rename_table :kinds, :genders
  end
end
