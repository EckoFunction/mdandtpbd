class AddDeclesinonToTerm < ActiveRecord::Migration
  def change
  	add_column :terms, :declension_id, :integer
  	add_index :terms, :declension_id
  end
end
