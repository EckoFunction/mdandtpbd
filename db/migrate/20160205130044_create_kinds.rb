class CreateKinds < ActiveRecord::Migration
  def change
    create_table :kinds do |t|
    	t.string :kind
		t.belongs_to :term, index: true

		t.timestamps
    end
  end
end
