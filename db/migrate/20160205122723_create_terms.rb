class CreateTerms < ActiveRecord::Migration
  def change
    create_table :terms do |t|
    	t.string :name
    	# t.belongs_to :similar_term, index: true

		t.timestamps
    end
  end
end
