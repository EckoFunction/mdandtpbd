class ChangeSimilarWordsToSimilarTerms < ActiveRecord::Migration
  def change
  	rename_table :similar_words, :similar_terms
  end
end
