class RemoveTermIdFromDeclensions < ActiveRecord::Migration
  def change
  	remove_column :declensions, :term_id
  end
end
