class CreateDeclensions < ActiveRecord::Migration
  def change
    create_table :declensions do |t|
		t.string :declension
		t.belongs_to :term, index: true

		t.timestamps
    end
  end
end
