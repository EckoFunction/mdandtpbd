class CreateSimilarWords < ActiveRecord::Migration
  def change
    create_table :similar_words do |t|
    	t.integer :parent_term_id
    	t.integer :child_term_id

		t.timestamps
    end
  end
end
