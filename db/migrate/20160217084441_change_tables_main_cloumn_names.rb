class ChangeTablesMainCloumnNames < ActiveRecord::Migration
  def change
  	rename_column :word_cases, :name, :content
  	rename_column :declensions, :declension, :content
  	rename_column :meanings, :value, :content
  	rename_column :endings, :ending, :content
  	rename_column :terms, :name, :content
  	rename_column :kinds, :kind, :content
  end
end
