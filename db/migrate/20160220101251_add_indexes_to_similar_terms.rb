class AddIndexesToSimilarTerms < ActiveRecord::Migration
  def change
  	add_index :similar_terms, :parent_term_id
  	add_index :similar_terms, :child_term_id
  end
end
