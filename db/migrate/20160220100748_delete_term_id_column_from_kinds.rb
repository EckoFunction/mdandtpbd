class DeleteTermIdColumnFromKinds < ActiveRecord::Migration
  def change
  	remove_column :kinds, :term_id
  end
end
