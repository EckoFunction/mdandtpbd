class CreateTermKinds < ActiveRecord::Migration
  def change
    create_table :term_kinds do |t|
    	t.integer :term_id, :index => true
    	t.integer :kind_id, :index => true

		t.timestamps
    end
  end
end
