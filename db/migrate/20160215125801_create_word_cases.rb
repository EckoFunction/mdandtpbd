class CreateWordCases < ActiveRecord::Migration
  def change
    create_table :word_cases do |t|
    	t.string :name
		t.belongs_to :declension, index: true

      	t.timestamps
    end
  end
end
