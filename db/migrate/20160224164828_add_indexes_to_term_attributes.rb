class AddIndexesToTermAttributes < ActiveRecord::Migration
  def change
  	add_index :terms, :kind_id
  	add_index :terms, :ending_id
  	add_index :terms, :term_number_id
  end
end
