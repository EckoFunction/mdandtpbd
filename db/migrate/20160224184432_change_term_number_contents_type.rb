class ChangeTermNumberContentsType < ActiveRecord::Migration
  def change
  	change_column :term_numbers, :content, :string
  end
end
