# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160226084329) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "declensions", force: :cascade do |t|
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "endings", force: :cascade do |t|
    t.string   "content"
    t.integer  "word_case_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "endings", ["word_case_id"], name: "index_endings_on_word_case_id", using: :btree

  create_table "genders", force: :cascade do |t|
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "meanings", force: :cascade do |t|
    t.text     "content"
    t.integer  "term_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "meanings", ["term_id"], name: "index_meanings_on_term_id", using: :btree

  create_table "similar_terms", force: :cascade do |t|
    t.integer  "parent_term_id"
    t.integer  "child_term_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "similar_terms", ["child_term_id"], name: "index_similar_terms_on_child_term_id", using: :btree
  add_index "similar_terms", ["parent_term_id"], name: "index_similar_terms_on_parent_term_id", using: :btree

  create_table "term_numbers", force: :cascade do |t|
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  create_table "terms", force: :cascade do |t|
    t.string   "content"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.integer  "gender_id"
    t.integer  "term_number_id"
    t.integer  "ending_id"
    t.integer  "declension_id"
  end

  add_index "terms", ["declension_id"], name: "index_terms_on_declension_id", using: :btree
  add_index "terms", ["ending_id"], name: "index_terms_on_ending_id", using: :btree
  add_index "terms", ["gender_id"], name: "index_terms_on_gender_id", using: :btree
  add_index "terms", ["term_number_id"], name: "index_terms_on_term_number_id", using: :btree

  create_table "word_cases", force: :cascade do |t|
    t.string   "content"
    t.integer  "declension_id"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "word_cases", ["declension_id"], name: "index_word_cases_on_declension_id", using: :btree

end
